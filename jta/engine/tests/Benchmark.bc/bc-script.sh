#!/bin/bash

tarball=bc-script.tar.gz

function test_build {
    echo "test compiling (should be here)"
}

function test_deploy {
	put bc-device.sh  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
    assert_define BENCHMARK_BC_EXPR1
    assert_define BENCHMARK_BC_EXPR2
    report "cd $JTA_HOME/jta.$TESTDIR; ./bc-device.sh $BENCHMARK_BC_EXPR1 $BENCHMARK_BC_EXPR1"  
}

. $JTA_ENGINE_PATH/scripts/benchmark.sh
