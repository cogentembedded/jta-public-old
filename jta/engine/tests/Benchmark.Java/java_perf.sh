tarball=java_perf.tar

function test_build {
    touch test_suite_ready
}

function test_deploy {
    put *.jar  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
    report "cd $JTA_HOME/jta.$TESTDIR; java -cp scimark2lib.jar jnt.scimark2.commandline"  
    report_append "cd $JTA_HOME/jta.$TESTDIR; java -jar dacapo-9.12-bach.jar avrora" $JTA_HOME/ 
    report_append "cd $JTA_HOME/jta.$TESTDIR; java -jar dacapo-9.12-bach.jar jython"  
    report_append "cd $JTA_HOME/jta.$TESTDIR; java -jar dacapo-9.12-bach.jar luindex"  
    report_append "cd $JTA_HOME/jta.$TESTDIR; java -jar dacapo-9.12-bach.jar lusearch"  
    #report_append "cd $JTA_HOME/jta.$TESTDIR; java -jar dacapo-9.12-bach.jar tomcat"  
    report_append "cd $JTA_HOME/jta.$TESTDIR; java -jar dacapo-9.12-bach.jar xalan"  
}

. $JTA_ENGINE_PATH/scripts/benchmark.sh
