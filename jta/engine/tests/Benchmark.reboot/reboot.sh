tarball=reboot

function test_build {
	true
}

function test_deploy {
	put $TEST_HOME/$tarball  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
	target_reboot
	report "cd $JTA_HOME/jta.$TESTDIR; ./reboot"  
}

. $JTA_ENGINE_PATH/scripts/benchmark.sh